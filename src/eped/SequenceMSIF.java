/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eped;

/**
 *3. Diseñar un interfaz SequenceMS<E> que defina las operaciones sobre
secuencias con un tamaño máximo limitado (Max Size). ¿Dónde situaría este
interfaz en el mapa de TAD de la asignatura?
* 
* 4. ¿Qué habría que modificar en los interfaces ListIF<E>, StackIF<E> y
QueueIF<E> para crear los interfaces ListMSIF<E>, StackMSIF<E> y
QueueMSIF<E> que extendiesen a SequenceMS<E>?
* 
* Creo que solo habría que extender a SeguenceMSIF
* 
 * @author regio
 */
public interface SequenceMSIF<E> extends SequenceIF<E>{
    
    /**
     * 
     * @return valor de la constante tamaño máximo
     */
    public int getMaxSize();
    /**
     * Haciendo uso de getMxSize() lanza una excepción cuando se supera en 1 el tamaño máximo
     * @throws NullPointerException 
     */
    public void isOverMaxSize() throws NullPointerException;
}
