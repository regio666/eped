/*
    Realizar una implementación de ListIPIF<E> (Listas con Punto de Interés)
    utilizando dos pilas como Estructura de Datos de soporte. Calcular el coste de
    todas las operaciones públicas de esta implementación.
 */
package eped;

/**
 *
 * @author regio
 * @param <E>
 */
public class ListIP<E> extends Collection<E>implements ListIPIF<E>{
    
    private Stack<E> prev;
    private Stack<E> post;
    
    public ListIP()
    {
        this.prev = new Stack<>();
        this.post = new Stack<>();
    }
    
    @Override
    public E getIP() {
        return post.getTop();
    }

    @Override
    public void nextIP() {
        this.post.push(this.prev.getTop());
    }

    @Override
    public void prevIP() {
        this.prev.push(this.post.getTop());
    }

    @Override
    public E get() {
        return this.getIP();
    }

    
    public void set(E e) {
        this.post.pop();
        this.post.push(e);
    }
    
    @Override
    public void insert(Object e) {
        this.post.push(e);
        super.size++; 
    }

    @Override
    public void remove() {
        this.post.pop();
        super.size--;
    }
    @Override
    public int size() {
        return super.size();
    }

    @Override
    public boolean isEmpty() {
        if (super.size() == 0)
            return true;
        else
            return false;
    }
    /**
     * Recorrido destructivo
     * coste: O(N)
     * @param e
     * @return 
     */
    @Override
    public boolean contains(Object e) {
        boolean encontrado = false;
        while (size > 0)
        {
            if(post.getTop() != e)
            {
                post.pop();
                size--;
            }
            else
            {
                encontrado = true;
            }
        }
        return encontrado;
    }

    @Override
    public void clear() {
        this.post.clear();
        this.prev.clear();
        super.size = 0;
    }

    @Override
    public IteratorIF<Object> iterator() {
        return new ListIPIterator();
    }

    private class ListIPIterator implements IteratorIF<Object>
    {

        private E ip;

        ListIPIterator()
        {
            this.ip = getIP();
        }

        public E getNext()
        {
            E elem = get();
            this.ip = getNext();
            return elem;
        }

        public boolean hasNext()
        {
            return this.ip != null;
        }

        public void reset()
        {
            this.ip = getIP();
        }
    }
}
