/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eped;

/**
 *2. Diseñar un interfaz ListHTIF<E> que defina las operaciones de las Listas
Primero-Siguiente (o Cabeza-Cola: Head-Tail) en las que el acceso y la
modificación sólo puede realizarse sobre el primer elemento de la lista. Todas
las operaciones, además, deberán devolver un valor (es decir, su tipo de salida
no puede ser void). ¿Dónde situaría este interfaz en el mapa de TAD de la
asignatura?  
 * @author regio
 */
public interface ListHTIF<E> extends SequenceIF{
        
        /**
         * Devuelve el elemento 
         * @post: getTail() !isEmpty()
         * @param posición de la cabeza is ListHTIF.size() > 0
         * @return E
         */
        public E getHead();
        /**
         * @post: getTail() !isEmpty()
         * @param posición de la cabeza is ListHTIF.size() > 1
         * @return ListHTIF
         */
        public ListHTIF getTail();
   
}
   
