/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eped;

/**
 *
 * @author regio
 */
public class List<E> extends Sequence<E> implements ListIF<E>{
    
    /* Constructor por defecto: crea una lista vacía */
    public List(){ super(); }
    /* Constructor por copia: delega en el constructor por copia *
    * de la secuencia */
    public List(List<E> s) { super(s); }

    @Override
    /* Devuelve el elemento pos-ésimo de la lista */
    public E get(int pos) {
    NodeSequence node = getNode(pos);
    return node.getValue();
    }

    @Override
    /* Modifica el elemento pos-ésimo de la lista */
   public void set(int pos, E e) {
   NodeSequence node = getNode(pos);
   node.setValue(e);
   }

    @Override
    /* Inserta un nuevo elemento en la lista en la posición *
    * indicada */
    public void insert(int pos, E elem) {
    NodeSequence newNode = new NodeSequence(elem);
    if(pos==1){
        newNode.setNext(this.firstNode);
        this.firstNode = newNode;
    }else{
        NodeSequence previousNode = getNode(pos-1);
        NodeSequence nextNode = previousNode.getNext();
        previousNode.setNext(newNode);
        newNode.setNext(nextNode);
    }
    this.size++;
    }

    @Override
    /* Elimina el elemento pos-ésimo de la lista */
    public void remove(int pos) {
        if(pos==1){
        this.firstNode = this.firstNode.getNext();
        }else{
            NodeSequence previousNode = getNode(pos-1);
            NodeSequence nextNode = previousNode.getNext().getNext();
            previousNode.setNext(nextNode);
        }
        this.size--;
    }

 
    
}
