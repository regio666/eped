/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eped;

/**
 * 
 * @author regio
 */
/* Representa una secuencia, que es una colección de *
* elementos que se organizan linealmente. */
public interface SequenceIF<E> extends CollectionIF<E> {
/* Devuelve el iterador sobre la secuencia. No necesita *
* parámetros puesto que el recorrido es lineal y único. */
public IteratorIF<E> iterator ();
}
