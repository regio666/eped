/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eped;

/**
 * 1. Diseñar un interfaz ListIPIF<E> que defina las operaciones de las Listas con
Punto de Interés (Interest Point). Recuerde que en este tipo de listas existe un
puntero que puede desplazarse hacia adelante y hacia atrás y que todas las
operaciones de consulta y modificación se realizan en la posición del puntero.
¿Dónde situaría este interfaz en el mapa de TAD de la asignatura?
 * @author regio
 */
public interface ListIPIF<E> extends SequenceIF<E>{
    
    /**
     * Devuelve al posición del punto de interes
     * @return 
     */
    public int getIP ();
    /**
     * Mueve el punto de interes un elemento
     * @pre: if getIP() !isEmpty()
     * @post: getIP() += 1
     */
    public void nextIP ();
    /**
     * Retrasa el punto de interes un elemento
     * @pre: if size() > 1 
     * @post: getIP() -= 1
     */
    public void prevIP ();
    /**
     * Devuelve el elemento en la posición del punto de interes
     * @pre: if size() !isEmpty();
     * @return E en la posicón getIP()  
     */
    public E get ();
    /**
     * Inserta el elemento pasado como parametro en la posic
     * @param e 
     */
    public void set (E e);
    /**
     * Vacía el elemento al que apunta el punto de interes 
     * pre: getIp() !isEmpty()
     * post: E get isEmpty()
     */
    public void remove ();
}
